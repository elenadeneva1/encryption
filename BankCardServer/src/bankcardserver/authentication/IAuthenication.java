package bankcardserver.authentication;

import bankcardmodel.User;
import java.sql.SQLException;

/**
 *
 * @author elena
 */
public interface IAuthenication {
    
    public void authenticateUser(User user) throws UserNotFound;
   
    public void createUser(User user) throws SQLException;
    
    public void close();
}
