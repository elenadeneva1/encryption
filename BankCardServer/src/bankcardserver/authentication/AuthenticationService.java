package bankcardserver.authentication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bankcardmodel.User;
import bankcardserver.encryption.PasswordEncryptor;

/**
 *
 * @author elena
 */
public class AuthenticationService implements IAuthenication {
    private static String jdbcDriver = "com.mysql.jdbc.Driver"; 
    private static String dbURL = "jdbc:mysql://localhost/BankToken";
    private static String getSql = "SELECT * FROM Users WHERE username=?";
    private static String saveSql = "INSERT INTO Users " +
            "(username, password, encryptRights, decryptRights)" +
            " VALUES (?, ?, ?, ?)";
      
    private static AuthenticationService service;
    
    public static IAuthenication getInstance() throws ClassNotFoundException, SQLException {
        if (service == null) {
            service = new AuthenticationService();
        }
        return service;
    }
    private Connection connection;
    private PreparedStatement getStmt;
    private PreparedStatement saveStmt;
    
    public AuthenticationService() throws ClassNotFoundException, SQLException {
        Class.forName(jdbcDriver);
        connection = DriverManager.getConnection(dbURL, "root", "lastik");
        getStmt = connection.prepareStatement(getSql);
        saveStmt = connection.prepareStatement(saveSql);
   }
    
    @Override
    public synchronized void authenticateUser(User user) throws UserNotFound {
        try {
            getStmt.setString(1, user.getName());
            ResultSet resultSet = getStmt.executeQuery();
            if (resultSet.next()) {
                String userPassword = PasswordEncryptor.decrypt(resultSet.getString("password"));
                if (!user.getPassword().equals(userPassword)) {
                    throw new UserNotFound(user);
                }
                
                if (resultSet.getBoolean("encryptRights")) {
                    user.enableEncrypting();
                } else {
                    user.disableEncrypting();
                }
                if (resultSet.getBoolean("decryptRights")) {
                    user.enableDecrypting();
                } else {
                    user.disableDecrypting();
                }
                
                if (resultSet != null) {
                    resultSet.close();
                }
            } else {
               throw new UserNotFound(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserNotFound(user);
        }
    }

    @Override
    public synchronized void createUser(User user) {
        try {
            getStmt.setString(1, user.getName());
            ResultSet resultSet = getStmt.executeQuery();
            if (resultSet.next()) {
                throw  new IllegalStateException("User could not be created");
            }
            saveStmt.setString(1, user.getName());
            saveStmt.setString(2, PasswordEncryptor.encrypt(user.getPassword()));
            saveStmt.setBoolean(3, user.hasEncryptRights());
            saveStmt.setBoolean(4, user.hasDecryptRights());
            saveStmt.executeUpdate();
        } catch (SQLException ex) {
            throw  new IllegalStateException("User could not be created");
        }
    }
    
    @Override
    public void close() {
        if (getStmt != null) {
            try {
                getStmt.close();
            } catch (SQLException ex) {}
        }
        if (saveStmt != null) {
             try {
                saveStmt.close();
            } catch (SQLException ex) {}
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {}
        }
    }
}