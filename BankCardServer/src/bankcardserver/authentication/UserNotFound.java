package bankcardserver.authentication;

import bankcardmodel.User;

/**
 *
 * @author elena
 */
public class UserNotFound extends Exception {

    private User user;
    
    public UserNotFound(User user) {
        this.user = user;
    }
    
    @Override
    public String getMessage() {
        return "User " + user.getName() + " not founUd";
    }
    
}
