package bankcardserver;

import java.io.IOException;

/**
 *
 * @author elena
 */
public class BancCardServerTest {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.keyStorePassword", "123456");
        System.setProperty("javax.net.ssl.keyStore","mySrvKeystore");
//        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//        System.setProperty("javax.net.debug","ssl");
        BankCardServer server = new BankCardServer(5000);
        try {
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
            server.stop();
        }
    }
}
