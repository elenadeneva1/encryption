package bankcardserver;

import java.io.IOException;
import java.net.Socket;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

/**
 *
 * @author elena
 */
public class BankCardServer {
    private int port;
    private SSLServerSocket server;
    private volatile boolean active;
    
    public BankCardServer(int port) {
        this.port = port;
        active = false;
    }
    
    public void start() throws IOException {
        SSLServerSocketFactory factory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        server = (SSLServerSocket) factory.createServerSocket(port);
        System.out.println("Server is listening...");
        active = true;
        Socket client;
        while(active) {
            try {
                try {
                    client = server.accept();
                    System.out.println("Client connected " + client.getInetAddress());
                } catch (IOException ex) {
                    if (!active) {
                        break;
                    }
                    throw new IOException(ex);
                }
                Thread thread = new Thread(new ClientRequest(client));
                thread.start();
            } catch (Exception e) {
                stop();
            }
        }
    }
    
    public void stop() {
        try {
            server.close();
            active = false;
            System.out.println("Server stopped");
        } catch (IOException e) {}
    }
}
