package bankcardserver.encryption;

/**
 *
 * @author elena
 */
public class EncryptMethod implements IEncryptable {
      
    private static final char encryptionMap[][] = new char[26][26];
    private static final char decryptionMap[][] = new char[26][26];
    static {
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                encryptionMap[i][(j + i) % 26] = (char) (j + 'a');
                decryptionMap[i][j] = (char) ((j + i) % 26 + 'a');
            }
        }
    }
    
    @Override
    public String encrypt(String plainText, String cipher) {
        char[] text = plainText.toCharArray();
        for (int i = 0; i < text.length; i++) {
            text[i] = encryptionMap[cipher.charAt(i % cipher.length())-'a'][text[i]-'a'];
        }
        return new String(text);
    }
    
    @Override
    public String decrypt(String encryptedText, String cipher) {
        char[] text = encryptedText.toCharArray();
        for (int i = 0; i < text.length; i++) {
            text[i] = decryptionMap[cipher.charAt(i % cipher.length())-'a'][text[i]-'a'];
        }
        return new String(text);
    }
}
