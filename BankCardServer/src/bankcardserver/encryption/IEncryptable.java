package bankcardserver.encryption;

/**
 *
 * @author elena
 */
public interface IEncryptable {
    
    public String encrypt(String plainText, String cipher);
    
    public String decrypt(String encryptedText, String cipher);
}
