package bankcardserver.encryption;

/**
 *
 * @author elena
 */
public class EncryptionService {
    
    public static EncryptionService getInstance() {
        return new EncryptionService(new EncryptMethod(), "asdasdw");
    }
    
    private IEncryptable encryption;
    private String cipher;
    
    public EncryptionService(IEncryptable encryption, String cipher) {
        this.encryption = encryption;
        this.cipher = cipher;
    }
    
    public String encrypt(String plainText) {
        return encryption.encrypt(plainText, cipher);
    }
    
     public String decrypt(String plainText) {
        return encryption.decrypt(plainText, cipher);
    }
}
