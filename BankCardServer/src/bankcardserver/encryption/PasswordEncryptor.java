package bankcardserver.encryption;

import org.jasypt.util.text.BasicTextEncryptor;

/**
 *
 * @author elena
 */
public class PasswordEncryptor {
        
    private static BasicTextEncryptor textEncryptor;
    
    static {
        textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword("asd");
    }
    
    public static String encrypt(String password) {
        return textEncryptor.encrypt(password);
    }
    
    public static String decrypt(String encryptedPassword) {
        return textEncryptor.decrypt(encryptedPassword);
    }       
}
