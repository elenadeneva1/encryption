package bankcardserver;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.sql.SQLException;

import com.thoughtworks.xstream.XStream;

import bankcardmodel.NewUserRequest;
import bankcardmodel.Request;
import bankcardmodel.TokenRequest;
import bankcardserver.authentication.AuthenticationService;
import bankcardserver.authentication.IAuthenication;
import bankcardserver.encryption.EncryptionService;
import bankcardmodel.User;

/**
 *
 * @author elena
 */
public class ClientRequest implements Runnable {
    private Socket client;
    private InputStream inputStream;
    private IAuthenication authenicationService;
    private EncryptionService encryptionService;
    
    public ClientRequest(Socket client) {
        this.client = client;
        try {
            inputStream = client.getInputStream();
            authenicationService = AuthenticationService.getInstance();
            encryptionService = EncryptionService.getInstance();
        } catch (Exception e) {
           e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            User user = getPrincipal();
            authenicationService.authenticateUser(user);
            send("OK\n");
            while(true) {
                Request request = getRequest();
                if (request == null) {
                    send("Request could not be completed\n");
                    continue;
                }
                if (request.getMethod().equals("exit")) {
                    send("OK\n");
                    close();
                    return;
                }
                if (validateRequest(user, request)) {
                    send(processRequest(request)+"\n");
                } else {
                    send("Not supported error or user not authorized!\n");
                }
            }
        } catch (Exception e) { 
            send("Error!\n");
            e.printStackTrace();
            close();
        }
    }

    private void close() {
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException ex) {}
        }
        if (client != null) {
            try {
                client.close();
            } catch (IOException ex) {}
        }
    }
    
    private User getPrincipal() throws IOException {
        byte[] buffer = new byte[1000];
        int bytesRead = inputStream.read(buffer);
        String user = new String(buffer, 0, bytesRead);
        return (User) new XStream().fromXML(user);
    }

    private Request getRequest() throws IOException {
        byte[] buffer = new byte[1000];
        int bytesRead;
        bytesRead = inputStream.read(buffer);
        if (bytesRead == -1) {
            return null;
        }
        String request = new String(buffer, 0, bytesRead);
        Object serializedObject = null;
        for (int i = 0; i < 5; i++) {
            try {
                serializedObject= new XStream().fromXML(request);
            } catch (Exception e) {
                bytesRead = inputStream.read(buffer);
                if (bytesRead == -1) {
                    return null;
                } else {
                    request += new String(buffer, 0, bytesRead);
                }
                continue;
            }
            break;
        }
        if (serializedObject instanceof TokenRequest) {
            return (TokenRequest)  serializedObject; 
        } else if (serializedObject instanceof NewUserRequest) {
            return (NewUserRequest) serializedObject; 
        } else if (serializedObject instanceof Request) {
            return (Request) serializedObject;
        }
        return null;
    }

    private boolean validateRequest(User user, Request request) {
        String method = request.getMethod();
       if (method.equals("encrypt") && user.hasEncryptRights() && !user.isAdmin()) {
           return true;
       } else if (method.equals("decrypt") && user.hasDecryptRights() && !user.isAdmin()) {
           return true;
       } else if (request.getMethod().equals("createUser") && user.isAdmin()){
           return true;
       } else {
           return false;
       }
    }
    
    private String processRequest(Request request) {
        if (request.getMethod().equals("encrypt")) {
           return encryptionService.encrypt(((TokenRequest) request).getText());
       } else if (request.getMethod().equals("decrypt")) {
           return encryptionService.decrypt(((TokenRequest) request).getText());
       } else if (request.getMethod().equals("createUser")){
            try {
                System.out.println("CREATE USER");
                authenicationService.createUser(((NewUserRequest) request).getUser());
                return "OK";
            } catch (SQLException ex) {
                return "Error";
            }
       } else {
           throw new IllegalStateException("Method not supported");
       }
    }

    private void send(String response) {
        try {
            client.getOutputStream().write(response.getBytes());
        } catch (IOException e) { }
    }
}
