package bankcardmodel;

/**
 *
 * @author elena
 */
public class Request {
   
     private String method;
    
     public Request(String method) {
         this.method = method;
     }
     
     public String getMethod() {
         return method;
     }    
}
