package bankcardmodel;

/**
 *
 * @author elena
 */
public class NewUserRequest extends Request {
  
    private User user;
    
    public NewUserRequest() {
        super("createUser");
        user = null;
    }
    
    public NewUserRequest(User user) {
        super("createUser");
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
}
