package bankcardmodel;

/**
 *
 * @author elena
 */
public class User {
    
    private String name;
    private String password;
    private boolean encryptRight;
    private boolean decryptRight;

    public User() {
        name = "";
        password = "";
        encryptRight = false;
        decryptRight = false;
    }
    
    public User(String name, String password) {
        this.name = name;
        this.password = password;
        encryptRight = false;
        decryptRight = false;
    }
    
    public User(String name, String password, boolean encryptRight, boolean decryptRight) {
        this.name = name;
        this.password = password;
        this.encryptRight = encryptRight;
        this.decryptRight = decryptRight;
    }
    
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    public void enableEncrypting() {
        encryptRight = true;
    }
    
    public void disableEncrypting() {
        encryptRight = false;
    }
    
    public boolean hasEncryptRights() {
        return encryptRight;
    }
        
     public void enableDecrypting() {
        decryptRight = true;
    }
    
    public void disableDecrypting() {
        decryptRight = false;
    }
    
    public boolean hasDecryptRights() {
        return decryptRight;
    }   

    public boolean isAdmin() {
        return name.equals("root");
    }
}
