package bankcardclient;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import bankcardmodel.User;

/**
 *
 * @author elena
 */
public class ConnectionPanel extends JPanel implements ActionListener {
    
    private JTextField host;
    private JLabel hostLabel;
    private JTextField port;
    private JLabel portLabel;    
    private JTextField username;
    private JLabel usernameLabel;
    private JPasswordField password;
    private JLabel passwordLabel;
    private JButton connect;
    
    public ConnectionPanel() {
        setLayout(new GridLayout(5, 2));
        setVisible(true);
        
        hostLabel = new JLabel("Host:", JLabel.CENTER);
        add(hostLabel);
        host = new JTextField();
        host.setText("127.0.0.1");
        add(host);
        
        portLabel = new JLabel("Port:", JLabel.CENTER);
        add(portLabel);
        port = new JTextField();
        port.setText("5000");
        add(port);
        
        usernameLabel = new JLabel("Username:", JLabel.CENTER);
        add(usernameLabel);
        username = new JTextField();
        add(username);
        
        passwordLabel = new JLabel("Password", JLabel.CENTER);
        add(passwordLabel);
        password = new JPasswordField();
        add(password);
        
        add(new JLabel());
        connect = new JButton("Connect");
        add(connect);
        connect.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent action) {
        try {
            User user = new User(username.getText(), new String(password.getPassword()));
            int portInt = Integer.parseInt(port.getText());
            BankCardClient.createInstance(host.getText(), portInt, user);
            if (user.isAdmin()) {
                getParent().add(new AdminPanel());
            } else {
                getParent().add(new EncryptionPanel());
            }
            getParent().validate();
            getParent().remove(this);
        } catch(Exception e) {
            BankCardClient.getInstance().disconnect();
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Exception: " + e.getMessage());
        }
    }
}
