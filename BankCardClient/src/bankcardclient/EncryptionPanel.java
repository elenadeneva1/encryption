package bankcardclient;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import bankcardmodel.Request;
import bankcardmodel.TokenRequest;

/**
 *
 * @author elena
 */
class EncryptionPanel extends JPanel {
    
    private BankCardClient client;
    private JButton encryptButton;
    private JButton decryptButton;
    private JButton disconnectButton;
    private JLabel textLabel;
    private JTextField text;
    private JLabel responseLabel;
    private JTextField response;
    private TokenRequest encryptRequest;
    private TokenRequest decryptRequest;
    
    public EncryptionPanel() {
        setVisible(true);
        setLayout(new GridLayout(4, 2));
        
        this.client = BankCardClient.getInstance();
        encryptRequest = new TokenRequest("encrypt", "");
        decryptRequest = new TokenRequest("decrypt", "");
        
        textLabel = new JLabel("Text: ", JLabel.CENTER);
        add(textLabel);
        
        text = new JTextField();
        add(text);
        
        encryptButton = new JButton("Encrypt");
        add(encryptButton);
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onEncrypt();
            }
        });
        
        decryptButton = new JButton("Decrypt");
        add(decryptButton);
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onDecrypt();
            }
        });
        
        responseLabel = new JLabel("Response:", JLabel.CENTER);
        add(responseLabel);
        response = new JTextField();
        add(response);
        
        add(new JLabel());
        disconnectButton = new JButton("Disconnect");
        add(disconnectButton);
        disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               onDisconnect();
            }
        });
    }
           
    private void onEncrypt() {
       encryptRequest.setText(text.getText());
       response.setText(client.sendRequest(encryptRequest));
    }
    
    private void onDecrypt() {
       decryptRequest.setText(text.getText());
       response.setText(client.sendRequest(decryptRequest));
    }
    
    private void onDisconnect() {
        client.sendRequest(new Request("exit"));
        client.disconnect();
        getParent().add(new ConnectionPanel());
        getParent().validate();
        getParent().remove(this);
    }
}
