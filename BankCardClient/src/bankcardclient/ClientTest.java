package bankcardclient;

import java.awt.event.WindowEvent;
import javax.swing.JFrame;

/**
 *
 * @author elena
 */
public class ClientTest {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.trustStorePassword", "123456");
        System.setProperty("javax.net.ssl.trustStore","mySrvKeystore");
//        System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
//        System.setProperty("javax.net.debug","ssl");
        JFrame application = new JFrame("Bank token encryption client");
        application.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(WindowEvent winEvt) {
                if (BankCardClient.getInstance() != null) {
                    BankCardClient.getInstance().disconnect();
                }
            }
        });
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);      
        application.add(new ConnectionPanel());
        application.setVisible(true);
        application.setSize(400, 400);
    }
}
