package bankcardclient;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import bankcardmodel.NewUserRequest;
import bankcardmodel.Request;
import bankcardmodel.User;

/**
 *
 * @author elena
 */
public class AdminPanel extends JPanel {
    
    private NewUserRequest request;
    private User user;
    private BankCardClient client;
    private JTextField username;
    private JLabel usernameLabel;
    private JTextField password;
    private JLabel passwordLabel;    
    private ButtonGroup encryptRightsGroup;
    private JRadioButton enableEncryptRights;
    private JRadioButton disableEncryptRights;
    private ButtonGroup decryptRightsGroup;
    private JRadioButton enableDecryptRights;
    private JRadioButton disableDecryptRights;
    private JLabel encryptRightsLabel;
    private JLabel decryptRightsLabel;
    private JButton createUserButton;
    private JButton disconnectButton;
    
    public AdminPanel() {
        request = new NewUserRequest();
        user = new User();
        user.disableDecrypting();
        user.disableEncrypting();
        client = BankCardClient.getInstance();
        setLayout(new GridLayout(8, 2));
        setVisible(true);
        
        usernameLabel = new JLabel("Username:", JLabel.CENTER);
        username = new JTextField();
        
        passwordLabel = new JLabel("Password:", JLabel.CENTER);
        password = new JTextField();
         
        initEncryptRights();
        initDecryptRights();
      
        createUserButton = new JButton("Create user");
        createUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               onCreateUser();
            }
        });
        
       disconnectButton = new JButton("Disconnect");
       disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               onDisconnect();
            }
        });
        add();
    }
    
    private void add() {
        add(usernameLabel);
        add(username);
        add(passwordLabel );
        add(password);
        add(encryptRightsLabel);
        add(new JLabel());
        add(enableEncryptRights);
        add(disableEncryptRights);
        add(decryptRightsLabel);
        add(new JLabel());
        add(enableDecryptRights);
        add(disableDecryptRights);
        add(new JLabel());
        add(createUserButton);
        add(new JLabel());
        add(disconnectButton);
    }
    
    private void onCreateUser() {
        user.setName(username.getText());
        user.setPassword(password.getText());
        request.setUser(user);
        String result = client.sendRequest(request);
        if (result.equals("OK")) {
            JOptionPane.showMessageDialog(null, "User added");
        } else {
            JOptionPane.showMessageDialog(null, "User was not added");
        }
    }
    
    private void onDisconnect() {
        client.sendRequest(new Request("exit"));
        client.disconnect();
        getParent().add(new ConnectionPanel());
        getParent().validate();
        getParent().remove(this);
    }

    private void initEncryptRights() {
        encryptRightsLabel = new JLabel("Encrypt rights", JLabel.CENTER);
        encryptRightsGroup = new ButtonGroup();
        enableEncryptRights = new JRadioButton("Enable encrypt rights");
        disableEncryptRights = new JRadioButton("Disable encrypt rights");
        disableEncryptRights.setSelected(true);
        
        encryptRightsGroup.add(enableEncryptRights);
        encryptRightsGroup.add(disableEncryptRights);
        
        enableEncryptRights.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               user.enableEncrypting();
            }
        });
        disableEncryptRights.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                user.disableEncrypting();
            }
        });
    }

    private void initDecryptRights() {
        decryptRightsLabel = new JLabel("Decrypt rights", JLabel.CENTER);
        decryptRightsGroup = new ButtonGroup();
        enableDecryptRights = new JRadioButton("Enable decrypt rights");
        disableDecryptRights = new JRadioButton("Disable decrypt rights");
        disableDecryptRights.setSelected(true);
        
        decryptRightsGroup.add(enableDecryptRights);
        decryptRightsGroup.add(disableDecryptRights);
               
        enableDecryptRights.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               user.enableDecrypting();
            }
        });
        disableDecryptRights.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                user.disableDecrypting();
            }
        });
    }
}
