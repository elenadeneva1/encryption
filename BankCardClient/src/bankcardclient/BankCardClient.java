package bankcardclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import com.thoughtworks.xstream.XStream;

import bankcardmodel.Request;
import bankcardmodel.User;

/**
 *
 * @author elena
 */
public class BankCardClient {

    private static BankCardClient instance;
    
    public static BankCardClient getInstance() {
        return instance;
    }
    
    public static void createInstance(String host, int port, User user) {
       if (instance != null) {
          instance.disconnect();
       }
       instance = new BankCardClient(host, port, user);
    }
    
    private SSLSocket client;
    PrintWriter out;
    BufferedReader in;
    
    public BankCardClient(String host, int port, User user) {
        boolean error = false;
        try {
            SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
            client = (SSLSocket) sslsocketfactory.createSocket(host, port);
            out = new PrintWriter(client.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(client.getInputStream()));
            XStream xstream = new XStream();
            String principals = xstream.toXML(user).toString();
            out.write(principals);
            out.flush();
            String response = in.readLine();
            if (response == null || !response.equals("OK")) {
                throw new IllegalStateException("User not connected!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException("User not connected!");
        }
    }
        
    public String sendRequest(Request token) {
        XStream xstream = new XStream();
        out.write(xstream.toXML(token).toString());
        out.flush();
        try {
            return in.readLine();
        } catch (IOException e) {
            return "Error";
        }
    }
    
    public void disconnect() {
        try {
            sendRequest(new Request("exit"));
            client.close();
        } catch (IOException e) {}
    }
}